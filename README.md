
# Python - les Comptes bancaires
## Sujet
Ecrire un programme qui implémente en POO un fonctionnement bancaire basique :  

- une classe abstraite Compte 
    - **attributs :** numero_compte, nom_proprietaire, solde  
    - **méthodes :** retrait, versement, afficher_solde  

- une classe fille CompteCourant, qui ajoute une gestion du découvert (montant maximum négatif 
possible) et des agios (pénalité de X % si le solde est inférieur à zéro) :  
    - **attributs :** autorisation_decouvert, pourcentage_agios  
    - **méthodes :** appliquer_agios  

- une classe fille CompteEpargne, qui ajoute :  
    - **attributs :** pourcentage_interets  
    - **méthodes :** appliquer_interets  

 ![Diagramme de classe](./diagramme_classe_v0.1.png)

Le programme doit demander à l’utilisateur le compte concerné (« courant » ou « epargne ») et le montant 
de la transaction (positif pour un versement, négatif pour un retrait)  

Chaque appel de méthode doit afficher le solde avant opération, le détail de l’opération et le solde après opération. On suppose pour la simplicité de l’exercice que chaque modification du solde applique les agios ou intérêts du compte modifié. 

## Interface

L'application doit pouvoir être lancée par le biais de deux interfaces. Une graphique (via TK), l'autre par le biais du terminal.

Il est recommandé d'utiliser une structure objet, avec héritage d'une classe abstraite pour implémenter ces interfaces utilisateur.

Vous trouverez ci-dessous les fonctionnalités minimales attendues des interfaces.

```mermaid
graph LR
    A[Connection par le biais d'un simple identifiant unique] -->|Identifiant connu| C
    A --> |Identifiant inconnu| A
    C{Accueil} -->|transaction cpte courant| D[Saisir le montant]
    C -->|transaction cpte épargne| D
    D -->|message info| C
    C --> E[Affichage solde]
    E --> C
```
